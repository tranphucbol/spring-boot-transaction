package org.bitmap.transaction.model;

import javax.persistence.*;

@Entity
@Table(name = "bank_account", uniqueConstraints = {
        @UniqueConstraint(columnNames = "user_name") // Khai báo ràng buộc unique cho thuộc tính username
})
public class BankAccount {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "user_name", nullable = false, length = 50)
    private String userName;

    @Column(nullable = false)
    private Double balance;

    public BankAccount() {
    }

    public BankAccount(String userName, Double balance) {
        this.userName = userName;
        this.balance = balance;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public Double getBalance() {
        return balance;
    }

    public void setBalance(Double balance) {
        this.balance = balance;
    }
}
