package org.bitmap.transaction.listener;

import org.bitmap.transaction.model.BankAccount;
import org.bitmap.transaction.repository.BankAccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationListener;
import org.springframework.context.event.ContextRefreshedEvent;
import org.springframework.stereotype.Component;

//Khởi tạo dữ liệu
@Component
public class DataSeedingListener implements ApplicationListener<ContextRefreshedEvent> {

    @Autowired
    private BankAccountRepository bankAccountRepository;

    @Override
    public void onApplicationEvent(ContextRefreshedEvent contextRefreshedEvent) {
        bankAccountRepository.deleteAll();
        //init
        if(bankAccountRepository.findByUserName("phuc") == null) {
            bankAccountRepository.save(new BankAccount("phuc", 100000d));
        }

        if(bankAccountRepository.findByUserName("nhut") == null) {
            bankAccountRepository.save(new BankAccount("nhut", 100000d));
        }
    }
}
