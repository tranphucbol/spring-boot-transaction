package org.bitmap.transaction.repository;

import org.bitmap.transaction.model.BankAccount;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BankAccountRepository extends JpaRepository<BankAccount, Long> {
    BankAccount findByUserName(String userName);
}
