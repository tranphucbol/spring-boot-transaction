package org.bitmap.transaction.service;

import org.bitmap.transaction.model.BankAccount;
import org.bitmap.transaction.repository.BankAccountRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;


@Service
public class BankAccountService {

    @Autowired
    private BankAccountRepository bankAccountRepository;

    @Transactional(propagation = Propagation.MANDATORY)
    public void addAmount(String userName, Double amount) throws Exception {
        BankAccount account = bankAccountRepository.findByUserName(userName);

        if(account == null)
            throw new Exception(userName + " not found");

        if(account.getBalance() + amount < 0)
            throw new Exception("Money of " + userName + " is not enough");

        account.setBalance(account.getBalance() + amount);
    }

    /*
        Transaction là một chuỗi hành động tương tác với database.
        Nếu một trong các hành động đó bị throw Exception thì các hành động thực hiện trước đó sẽ được Rollback

        Ở đây ta có một addAmount là một hành động thêm amount vào balance.
        Exception ở đây là:
            - Account có thể k tồn tại
            - Balance còn lại không đủ
        Nếu toUser được amount thì balance trong toUser sẽ được tăng.
        sau đó fromUser phải trừ đi amount tương ứng nếu balance trong fromUser không đủ sẽ có Exception
        và rollback balance của toUser
     */
    @Transactional(rollbackFor = Exception.class, propagation = Propagation.REQUIRES_NEW)
    public void transfer(String fromUser, String toUser, Double amount) throws Exception {
        addAmount(toUser, amount);
        addAmount(fromUser, -amount);
    }
}
