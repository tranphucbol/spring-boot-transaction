package org.bitmap.transaction.controller;

import org.bitmap.transaction.form.TransferForm;
import org.bitmap.transaction.model.BankAccount;
import org.bitmap.transaction.repository.BankAccountRepository;
import org.bitmap.transaction.service.BankAccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

@Controller
public class HomeController {

    @Autowired
    private BankAccountRepository bankAccountRepository;

    @Autowired
    private BankAccountService bankAccountService;

    @RequestMapping(value = {"/", "home"})
    public String homeView(Model model) {
        List<BankAccount> bankAccounts = bankAccountRepository.findAll();
        model.addAttribute("transferForm", new TransferForm("phuc", "nhut", 1000d));
        model.addAttribute("bankAccounts", bankAccounts);
        return "home";
    }

    @RequestMapping(value = "/transfer", method = RequestMethod.POST)
    public String transferProcessing(@ModelAttribute("transferForm")TransferForm transferForm,
                                     Model model) {
        try {
            bankAccountService.transfer(transferForm.getFromUser(), transferForm.getToUser(), transferForm.getAmount());
        } catch (Exception e) {
            e.printStackTrace();
            String error = "Error: " + e.getMessage();
            return "redirect:/home?error=" + error;
        }
        return "redirect:/";
    }
}
